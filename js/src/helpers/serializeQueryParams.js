export function serializeQueryParams(obj) {
  return '?' + Object.keys(obj).map(k => k + '=' + encodeURIComponent(obj[k])).join('&');
}