export function getImageMacro(layout) {
  let imageMacro = '';
  switch(layout) {
    case 'edge':
      imageMacro = 't_cms_big';
      break;
    default:
      imageMacro = 't_cms_small';
  }
  return imageMacro;
}