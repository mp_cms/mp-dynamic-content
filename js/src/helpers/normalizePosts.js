import JSONPath from 'jsonpath';

export function normalizePosts(response, map) {
  const rawPosts = JSONPath.query(
    response,
    map.list
  );

  return rawPosts.map(post => {
    return {
      title: JSONPath.query(
        post,
        map.title
      )[0],
      image: JSONPath.query(
        post,
        map.image
      )[0],
      url: JSONPath.query(
        post,
        map.url
      )[0],
    };
  });
}