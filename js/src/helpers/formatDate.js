export function formatDate(time) {
	var weekday = ["SUN","MON","TUE","WED","THU","FRI","SAT"];

	var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
	"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	var timestamp = Date.parse(time);
	var date = new Date(timestamp);
	var currentdate = new Date();

	// day from the timestamp
	var day = weekday[date.getDay()];
	var dom = date.getDate();
	var month = monthNames[date.getMonth()]
	// Hours part from the timestamp
	var hours = date.getHours();
	var meridiem = "PM";
	if (hours > 12) {
			hours -= 12;
			meridiem = "PM";
	} else if (hours === 0) {
		 hours = 12;
		 meridiem = "AM";
	}
	// Minutes part from the timestamp
	var minutes = "0" + date.getMinutes();

	var date_tomorrow = new Date(currentdate.getFullYear(), currentdate.getMonth(), currentdate.getDate() + 1);

	var show_date;
	if(date.getFullYear() === currentdate.getFullYear() &&
         date.getMonth() === currentdate.getMonth() &&
         date.getDate() === currentdate.getDate()) {
		show_date = "Tonight";
	} else if(date.getFullYear() === date_tomorrow.getFullYear() &&
         date.getMonth() === date_tomorrow.getMonth() &&
         date.getDate() === date_tomorrow.getDate()) {
		show_date = "Tomorrow";
	} else {
		show_date = day + " " + month + " " + dom;
	}

	return show_date + " - " + hours + ':' + minutes.substr(-2) + meridiem;
};
