export function formatParams(staticParams, deviceParams) {
  let params = {};

  // Add fixed-value query parameters to API URL.
  staticParams.forEach(function(param) {
    params[param.param] = param.value;
  });

  // Add device parameter values to API URL.
  // Note: the device bridge is only present in the Drupal theme.
  if (window.MobilePosse) {
    deviceParams.forEach(function(param) {
      // Call device function using provided function name
      const method = param.value;
      if (typeof window.MobilePosse[method] === 'function') {
        const deviceValue = window.MobilePosse[method]();
        if (typeof deviceValue === 'string') {
          params[param.param] = window.MobilePosse[method]();
        }
      }
      else {
        console.error('MobilePosse.' + method + '() is not a function.');
      }
    });
  }
  return params;
}