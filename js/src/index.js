import React from 'react';
import ReactDOM from 'react-dom';
import './styles.css';
import Block from './components/Block';
import {vendorProvider} from './components/vendorProvider';

const settings = window.drupalSettings.mp_dynamic_content || {};

Object.keys(settings.blocks).forEach(key => {
  const VendorBlock = vendorProvider(Block, settings.blocks[key].vendor);
  ReactDOM.render(
    <VendorBlock settings={settings.blocks[key]}/>,
    document.getElementById(key)
  );
});
