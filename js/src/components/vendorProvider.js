import React from 'react';

export function vendorProvider(Component, vendor = 'default') {
  return class VendorProvider extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        vendorModule: null
      }
    }

    async componentDidMount() {
      const VendorModule = await import(`../vendors/${vendor}`);
      this.setState({
        vendorModule: VendorModule
      })
    }

    render() {
      return <Component vendor={this.state.vendorModule} {...this.props}/>
    }
  };
}