import React from 'react';
import axios from 'axios';
import jsonp from 'jsonp';
import VisibilitySensor from 'react-visibility-sensor';
import { normalizePosts } from '../helpers/normalizePosts';
import { formatParams } from '../helpers/formatParams';
import { serializeQueryParams } from '../helpers/serializeQueryParams';
import Posts from './Posts';

class Block extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
    this.onVisibilityChange = this.onVisibilityChange.bind(this);

    this.state = {
      posts: [],
      loading: false,
      isVisible: false,
      seen: false,
      error: null,
    };
  }

  getJsonPathMap() {
    const settings = this.props.settings;

    if (this.props.vendor && this.props.vendor.pathMap) {
      return this.props.vendor.pathMap(this.props);
    }

    return {
      list: settings.path_to_content_list,
      title: settings.subpath_to_content_title,
      image: settings.subpath_to_content_image,
      url: settings.subpath_to_content_url
    };
  }

  localFetch() {
    // Allow vendors to fetch the data locally.
    if (this.props.vendor.localFetch) {
      const response = this.props.vendor.localFetch(this.props);
      if (response) {
        const cleanPosts = normalizePosts(response, this.getJsonPathMap());
        this.setState({
          ...this.state,
          posts: cleanPosts,
          loading: false,
          error: null,
        });
        return true;
      }
    }
    return false;
  }

  fetchJSON(url, params) {
    this.setState({
      ...this.state,
      loading: true,
    });

    const options = {
      params: params,
    };

    axios
      .get(url, options)
      .then(res => {
        // Allow vendors to modify or react to the response.
        if (this.props.vendor.postFetch) {
          const modifiedResponse = this.props.vendor.postFetch(res.data, this.props);
          res.data = modifiedResponse ? modifiedResponse : res.data;
        }

        var cleanPosts;
        if (this.props.vendor.normalizePosts) {
          cleanPosts = this.props.vendor.normalizePosts(res.data, this.getJsonPathMap());
        }
        else {
          cleanPosts = normalizePosts(res.data, this.getJsonPathMap()); // TODO: make mandatory function in vendor's postFetch()? ie: remove from Drupal?
        }

        this.setState({
          ...this.state,
          posts: cleanPosts,
          loading: false,
          error: null,
        });
      })
      .catch(err => {
        this.setState({
          ...this.state,
          loading: false,
          error: err,
        });
      });
  }

  fetchJSONP(url, params) {
    this.setState({
      ...this.state,
      loading: true,
    });

    params = serializeQueryParams(params);

    jsonp(url + params, null, (err, data) => {
      if (err) {
        this.setState({
          loading: false,
          error: err,
        });
        console.error(err.message);
      } else {
        // Allow vendors to modify or react to the response.
        if (this.props.vendor.postFetch) {
          const modifiedResponse = this.props.vendor.postFetch(data, this.props);
          data = modifiedResponse ? modifiedResponse : data;
        }

        var cleanPosts;
        if (this.props.vendor.normalizePosts) {
          cleanPosts = this.props.vendor.normalizePosts(data, this.getJsonPathMap());
        }
        else {
          cleanPosts = normalizePosts(data, this.getJsonPathMap());
        }

        this.setState({
          ...this.state,
          posts: cleanPosts,
          loading: false,
          error: null,
        });
      }
    });
  }

  componentDidMount() {
    // Nothing any more?
  }

  renderLoading() {
    return <div>{'Loading ' + this.props.settings.vendor}</div>;
  }

  renderError() {
    return <div>Error: {this.state.error.message}</div>;
  }

  handleClick(e) {
    // Vendor-specific callback.
    this.props.vendor.onClick && this.props.vendor.onClick(e);

    if (this.props.settings.chrome_links && window.MobilePosse) {
      e.preventDefault();
      window.MobilePosse.openCustomTab(e.currentTarget.href);
    }
  }

  fetchData() {
    let { url, query_parameters, device_parameters } = this.props.settings;
    const settings = this.props.settings;
    let params = formatParams(query_parameters, device_parameters);

    // Allow vendors to modify URL and params.
    if (this.props.vendor.preFetch) {
      const modifiedArgs = this.props.vendor.preFetch(url, params, this.props);
      // Use any returned values if they exist.
      if (modifiedArgs) {
        url = modifiedArgs.url ? modifiedArgs.url : url;
        params = modifiedArgs.params ? modifiedArgs.params : params;
      }
    }

    // TODO: returns false for subsequent taboola Blocks that are also visible on initial page load, because local storage is not set yet.
    if (this.localFetch()) return;

    // Fetch content.
    if (settings.jsonp) {
      this.fetchJSONP(url, params);
    }
    else {
      this.fetchJSON(url, params);
    }
  }

  onVisibilityChange(isVisible) {
    const lazyLoad = this.props.settings.lazy_load;
    // Fetch data if this is the first time the block is being seen.
    const shouldFetch = ((lazyLoad && isVisible) || !lazyLoad) &&
      !this.state.loading &&
      this.state.posts.length === 0 &&
      this.state.seen === false;

    this.props.vendor.onVisibilityChange && this.props.vendor.onVisibilityChange(isVisible, this.props, shouldFetch);
    this.setState({
      ...this.state,
      isVisible
    });
    if (shouldFetch) {
      this.setState({
        ...this.state,
        seen: true
      });
      this.fetchData();
    }
  }

  getContainerClasses() {
    return [
      'mp_dynamic_content',
      this.props.settings.layout,
      this.props.settings.vendor
    ].join(' ');
  }

  renderPosts() {
    if (this.state.error) {
      return this.renderError();
    }

    if (this.props.vendor && this.props.vendor.Template && this.state.posts.length > 0) {
      const VendorTemplate = this.props.vendor.Template;
      return (
        <VendorTemplate {...this.props} posts={this.state.posts} handleClick={this.handleClick} />
      )
    }

    return (
      <div>
        <Posts {...this.props} posts={this.state.posts} handleClick={this.handleClick} />
      </div>
    );
  }

  render() {
    // Give block a minimum height so that visibility does not change as results come in.
    const postheight = this.props.settings.layout === 'edge' ? 195 : 95;
    const minHeight = {
      minHeight: (this.props.settings.layout === 'carousel') ? '95px' : this.props.settings.count * postheight + 'px'
    };

    return (
      <VisibilitySensor offset={{ bottom: -100 }} active={this.props.vendor} onChange={this.onVisibilityChange} partialVisibility={true} delayedCall={false}>
        <div style={minHeight} className={this.getContainerClasses()}>
          {this.state.loading ? this.renderLoading() : this.renderPosts()}
        </div>
      </VisibilitySensor>
    );
  }
}

export default Block;
