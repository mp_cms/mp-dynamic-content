import React from 'react';
import Slider from "react-slick";
import '../helpers/trunc';
import { getImageMacro } from '../helpers/getImageMacro';

class Posts extends React.Component {

  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e) {
    this.props.handleClick(e);
  }

  render() {
    const imageResizeURL = '//res.images.mobileposse.com/mpcashew/image/fetch/';

    if (this.props.settings.layout === 'carousel') {
      var settings = {
        accessibility: true,
        dots: false,
        slidesToShow: 2,
        slidesToScroll: 1,
        arrows: false,
        infinite: true,
        speed: 500
      };

      return (
        <div className="container">
          <Slider {...settings}>
            {this.props.posts.map((post, i) => (
              <div>
                <a key={i} href={post.url} onClick={this.handleClick}>
                  <img src={imageResizeURL + getImageMacro(this.props.settings.layout) + '/' + post.image} alt="" />
                  <div className="title">{post.title.trunc(100, true)}</div>
                </a>
              </div>
            ))}
          </Slider>
        </div>
      )
    }

    return (
      <section>
        {this.props.posts.map((post, i) => (
          <a key={i} href={post.url} onClick={this.handleClick}>
            <img src={imageResizeURL + getImageMacro(this.props.settings.layout) + '/' + post.image} alt="" />
            <div className="title">{post.title.trunc(100, true)}</div>
          </a>
        ))}
      </section>
    )
  }
}

export default Posts;
