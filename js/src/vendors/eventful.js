import React from 'react';
import Slider from "react-slick";
import '../helpers/trunc';
import { formatDate } from '../helpers/formatDate';
import { getImageMacro } from '../helpers/getImageMacro';
import JSONPath from 'jsonpath';

export function preFetch(url, params, props) {
  const lat = localStorage.getItem('mp_loc_lat');
  const lng = localStorage.getItem('mp_loc_lng');
  const locality = localStorage.getItem('mp_addr_mLocality');
  const state = localStorage.getItem('mp_addr_Address_ST');
  if (locality && state) {
    var loc = locality + ', ' + state;
  }

  let newParams = {};
  if (loc) {
    newParams = {
      loc
    }
  }
  return {
    params: {
      ...params,
      ...newParams,
      app_key: props.settings.api_key,
    }
  };
}

// Override generic map.
export function pathMap(props) {
  //const settings = props.settings;

  return {
    list: '$.*',
    title: '$.title',
    image: '$.image.block67.url',
    url: '$.url',
    venue: '$.venue_name',
    date: '$.start_time'
  };
}

export function normalizePosts(response, map) {
  const rawPosts = JSONPath.query(
    response,
    map.list
  );

  return rawPosts.map(post => {
    return {
      title: JSONPath.query(
        post,
        map.title
      )[0],
      image: JSONPath.query(
        post,
        map.image
      )[0],
      url: JSONPath.query(
        post,
        map.url
      )[0],
      venue: JSONPath.query(
        post,
        map.venue
      )[0],
      date: JSONPath.query(
        post,
        map.date
      )[0],
    };
  });
}

/**
 * Custom Outbrain template.
 */
export class Template extends React.Component {

  constructor(props){
    super(props);
    // Check where to start if we are not the first eventful block to render on the page.
    const start = window.eventful ? window.eventful : 0;
    // Update offset for any subsequent eventful blocks to use.
    if (typeof window.eventful === 'undefined') {
      window.eventful = this.props.settings.count;
    } else {
      window.eventful += this.props.settings.count;
    }
    this.state = {
      start: start
    }
  }

  renderHeader() {
    return (
        <header>
          <div className="content-left">
            <h2 className="title">
            Events by
            </h2>
            <div className="provider_logo">
            <img alt="Eventful"
              src="//res.cloudinary.com/mpcashew/image/upload/c_scale,w_200/v1542126132/eventful_logo_t9teug.png"
              width="52"
              height="12" />
            </div>
            </div>
            <div className="content-right">
            <div className="location">
            New York, NY
            </div>
            <div className="arrow">
            <img alt="arrow" src="https://res.cloudinary.com/mpcashew/image/upload/v1542151080/arrow_ocdvqx.png"
             width="12" height="12" />
            </div>
            </div>
        </header>
    );
  }

  renderCarousel() {
    const settings = {
      accessibility: true,
      dots: false,
      slidesToShow: 1.8,
      slidesToScroll: 1,
      swipeToSlide: true,
      arrows: false,
      infinite: false,
      speed: 500
    };
    var items = this.props.posts.slice(this.state.start, parseInt(this.props.settings.count,10) + parseInt(this.state.start,10));
    return (
      <Slider {...settings}>
        {items.map((post, i) => (
          <div key={i}>{this.renderPost(post)}</div>
        ))}
      </Slider>
    )
  }

  renderPost(post) {
    const imageResizeURL = '//res.images.mobileposse.com/mpcashew/image/fetch/';
    return (
      <a href={post.url} onClick={this.handleClick}>
        <img src={imageResizeURL + getImageMacro(this.props.settings.layout) + '/' + post.image} alt="" />
        <div className="title">{post.title.trunc(100, true)}</div>
          <div className="item-footer">
          <p className="start-time">{formatDate(post.date)}</p>
          <p className="venue">{post.venue}</p>
          </div>
      </a>
    )
  }

  renderPosts() {
    var items = this.props.posts.slice(this.state.start, parseInt(this.props.settings.count,10) + parseInt(this.state.start,10));
    return (
      <div>
        {items.map((post, i) => (
          <div key={i}>{this.renderPost(post, i)}</div>
        ))}
      </div>
    )
  }

  render() {
    return (
      <section>
        {this.props.settings.show_provider_branding === 1 ? this.renderHeader() : null}
        {this.props.settings.layout === 'carousel' ? this.renderCarousel() : this.renderPosts()}
      </section>
    )
  }
}
