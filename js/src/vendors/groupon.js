import React from 'react';
import Slider from "react-slick";
import '../helpers/trunc';
import { getImageMacro } from '../helpers/getImageMacro';
import JSONPath from 'jsonpath';

export function preFetch(url, params, props) {
  const lat = localStorage.getItem('mp_loc_lat');
  const lng = localStorage.getItem('mp_loc_lng');

  let newParams = {};
  if (lat && lng) {
    newParams = {
      'latitude': lat,
      'longitude': lng
    }
  }
  return {
    params: {
      ...params,
      ...newParams,
      tsToken: props.settings.api_key,
      limit: props.settings.count,
    }
  };
}

// Override generic map.
export function pathMap(props) {
  //const settings = props.settings;

  return {
    list: '$.deals[*]',
    title: '$.title',
    image: '$.img',
    url: '$.url',
    vendor: '$.merchant',
    location: '$.location',
    price: '$.price'
  };
}

export function normalizePosts(response, map) {
  const rawPosts = JSONPath.query(
    response,
    map.list
  );

  return rawPosts.map(post => {
    return {
      title: JSONPath.query(
        post,
        map.title
      )[0],
      image: JSONPath.query(
        post,
        map.image
      )[0],
      url: JSONPath.query(
        post,
        map.url
      )[0],
      vendor: JSONPath.query(
        post,
        map.vendor
      )[0],
      location: JSONPath.query(
        post,
        map.location
      )[0],
      price: JSONPath.query(
        post,
        map.price
      )[0],
    };
  });
}

/**
 * Custom Groupon template.
 */
export class Template extends React.Component {

  renderHeader() {
    //const locale = localStorage.getItem('mp_addr_Address_ST');

    return (
      <header>
        <a href="https://www.groupon.com">
          <img src="http://assets1.grouponcdn.com/images/logos/powered_by_groupon.png" alt="Groupon Logo"
            className="groupon-logo" />
        </a>
      </header>
    );
  }

  renderPost(post) {
    const imageResizeURL = '//res.images.mobileposse.com/mpcashew/image/fetch/';
    return (
      <a href={post.url} onClick={this.handleClick}>
        <img src={imageResizeURL + getImageMacro(this.props.settings.layout) + '/' + post.image} alt="" />
        <div className="title">{post.title.trunc(100, true)}</div>
        <div className="info">
          <span className="vendor">{post.vendor} 	&bull; {post.location}</span>
        </div>
      </a>
    )
  }

  renderPosts() {
    return (
      <div>
        {this.props.posts.map((post, i) => (
          <div key={i}>{this.renderPost(post, i)}</div>
        ))}
      </div>
    )
  }

  renderCarousel() {
    const settings = {
      accessibility: true,
      dots: false,
      slidesToShow: 1.8,
      slidesToScroll: 1,
      swipeToSlide: true,
      arrows: false,
      infinite: true,
      speed: 500
    };
    return (
      <Slider {...settings}>
        {this.props.posts.map((post, i) => (
          <div key={i}>{this.renderPost(post)}</div>
        ))}
      </Slider>
    )
  }

  render() {
    return (
      <section>
        {this.props.settings.show_provider_branding === 1 ? this.renderHeader() : null}
        {this.props.settings.layout === 'carousel' ? this.renderCarousel() : this.renderPosts()}
      </section>
    )
  }
}