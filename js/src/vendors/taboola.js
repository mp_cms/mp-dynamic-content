import React from 'react';
import Posts from '../components/Posts';
import axios from 'axios';

function getSession() {
  const session = JSON.parse(sessionStorage.getItem('taboola-session'));

  if(session) {
    const SIX_HOURS = 6 * 60 * 60 * 1000;

    // If the session is over six hours old, delete it and stored results.
    if(((Date.parse(new Date())) - Date.parse(session.expiresAt)) > SIX_HOURS) {
      window.sessionStorage.removeItem('taboola-session');
      window.sessionStorage.removeItem('taboola-result');
      console.log('Taboola session has been reset.');
    }
    else {
      return session.result;
    }
  }
  return 'init';
}

function getPlacementParams() {
  // Loop through all taboola blocks, combining the requests into multiple "placements" so that content is not
  // duplicated between multiple requests
  // TODO: settings should be an argument.
  const settings = window.drupalSettings.mp_dynamic_content.blocks || {};
  let placement = {};
  let count = 1;
  Object.keys(settings).forEach((key, i) => {
    if (settings[key].vendor === 'taboola') {
      placement[`placement${count}.rec-count`] = settings[key].count;
      placement[`placement${count}.organic-type`] = 'mix';
      placement[`placement${count}.available`] = true;
      placement[`placement${count}.visible`] = count === 1; // Assuming only the first block will be above the fold.
      placement[`placement${count}.name`] = settings[key].placement_name;
      count++;
    }
  });
  return placement;
}

function notifyVisible(apikey, session, id) {
  const options = {
    params: {
      'app.type': 'mobile',
      'app.apikey': apikey,
      'response.session': session,
      'response.id': id
    }
  }
  axios.get('https://api.taboola.com/1.2/json/mp-metropcs-cms/recommendations.notify-visible', options);
}

export function preFetch(url, params, props) {
  const placement = getPlacementParams();

  return {
    params: {
      ...params,
      ...placement,
      'user.session': getSession(),
      'device.id': window.MobilePosse ? window.MobilePosse.getDeviceID() : '',
      'source.type': 'section',
      'app.type': 'mobile',
      'app.apikey': props.settings.api_key,
    }
  };
}

export function postFetch(response, props) {
  // Save entire response.
  if (response.session) {
    var expires = new Date(Date.now());
    try {
      // Set an expiration date in the taboola-session
      // This will be used to reinitialize the session every six hours
      var sessionObject = {
        expiresAt: expires,
        result: response.session,
      }

      window.sessionStorage.setItem('taboola-result', JSON.stringify(response));
      window.sessionStorage.setItem('taboola-session', JSON.stringify(sessionObject));
    }
    catch ($e) {}
  }
  const result = response[props.settings.placement_name];
  // Notify Taboola that this block is visible.
  if (props.settings.lazy_load) {
    notifyVisible(props.settings.api_key, response.session, result.id);
  }
  // Return only the results for the current block.
  return result;
}

export function onClick() {
  console.log('taboola onClick');
}

export function onVisibilityChange(isVisible, props, firstView) {
  // Notify Taboola if block is visible and has just been seen for first time.
  let result = sessionStorage.getItem('taboola-result');
  if (isVisible && props.settings.lazy_load && result && firstView) {
    result = JSON.parse(result);
    if (result[props.settings.placement_name]) {
      notifyVisible(props.settings.api_key, result.session, result[props.settings.placement_name].id);
    }
  }
}

export function localFetch(props) {
  // Fetch response from session storage, if it exists.
  const result = JSON.parse(sessionStorage.getItem('taboola-result'));
  const key = props.settings.placement_name;
  if (result && result[key]) {
    return result[key];
  }
  return null;
}

/**
 * Custom Taboola template.
 */
export class Template extends React.Component {
  render() {
    return (
      <div className="taboola-posts">
        <Posts {...this.props}/>
      </div>
    )
  }
}
