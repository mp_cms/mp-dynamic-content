export function preFetch(url, params, props) {
  return {
    params: {
      ...params,
      count: props.settings.count,
    }
  };
}

export function postFetch(response) { }

export function onClick(e) { }

export function onVisibilityChange(isVisible) { }
