import React from 'react';
import Posts from '../components/Posts';

export function preFetch(url, params, props) {
  return {
    params: {
      ...params,
      key: props.settings.api_key,
      widgetJSId: props.settings.placement_name,
      isSecured: (window.location.protocol === 'https:') ? true : false,
      userId: window.MobilePosse ? window.MobilePosse.getAdvertisingID() : ''
    }
  }
}

/**
 * Custom Outbrain template.
 */
export class Template extends React.Component {
  render() {
    return (
      <div>
        {this.props.settings.show_provider_branding &&
          <header>
            <span>Recommended by</span>
            <a href="https://www.outbrain.com/what-is/?uid=abc123">
              <img src="https://widgets.outbrain.com/images/widgetIcons/ob_logo_67x12.png" alt="Outbrain Logo"
                   className="outbrain-logo"/>
            </a>
            <span>|</span>
            <a href="https://www.outbrain.com/what-is/?uid=abc123">
              <img src="https://widgets.outbrain.com/images/widgetIcons/achoice.svg" alt="Ad Choices"
                   className="ad-choices"/>
            </a>
          </header>
        }
        <Posts {...this.props}/>
      </div>
    )
  }
}
