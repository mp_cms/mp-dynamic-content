<?php

namespace Drupal\mp_dynamic_content\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Term;

/**
 * Provides a 'DynamicContentBlockSimple' block.
 *
 * @Block(
 *  id = "dynamic_content_block_simple",
 *  admin_label = @Translation("Dynamic content block - simple"),
 * )
 */
class DynamicContentBlockSimple extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    // Get list of available providers.
    $term_data = [];
    $vid = 'providers';
    $terms = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadTree($vid);

    foreach ($terms as $term) {
      $term_data[$term->tid] = $term->name;
    }

    // Selector for the provider type.
    $form['provider_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Provider'),
      '#options' => $term_data,
      '#default_value' => isset($this->configuration['provider_type']) ? $this->configuration['provider_type'] : '',
      '#required' => TRUE,
      '#attributes' => [
        'class' => ['provider_type'],
      ],
    ];

    $form['query_parameters'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Arbitrary parameters for API URL'),
      '#description' => $this->t('API URL query parameters that have fixed values in the format <b>parameter|value</b><br>eg: app.type|mobile'),
      '#default_value' => isset($this->configuration['query_parameters']) ? $this->configuration['query_parameters'] : '',
    ];

    $form['count'] = [
      '#type' => 'number',
      '#title' => $this->t('Count'),
      '#description' => $this->t('Number of pieces of content to show.'),
      '#default_value' => isset($this->configuration['count']) ? $this->configuration['count'] : 1,
    ];

    $form['placement_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Placement Name'),
      '#description' => $this->t('Name of content slot. Often sent to the content provider.'),
      '#default_value' => isset($this->configuration['placement_name']) ? $this->configuration['placement_name'] : 'PLACEMENT_NAME',
    ];

    $form['layout'] = [
      '#type' => 'select',
      '#title' => $this->t('Layout'),
      '#options' => ['default' => 'Default', 'edge' => 'Edge to Edge', 'carousel' => 'Carousel'],
      '#default_value' => isset($this->configuration['layout']) ? $this->configuration['layout'] : 'default',
    ];

    $form['show_provider_branding'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show Provider Branding'),
      '#description' => $this->t('Enable this to show or remove provider branding (if supported by provider).'),
      '#default_value' => isset($this->configuration['show_provider_branding']) ? $this->configuration['show_provider_branding'] : FALSE,
    ];

    $form['lazy_load'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Lazy Load'),
      '#description' => $this->t('Only fetch the data and render the block when it is visible in the viewport.'),
      '#default_value' => isset($this->configuration['lazy_load']) ? $this->configuration['lazy_load'] : FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->configuration['provider_type'] = $values['provider_type'];
    $this->configuration['query_parameters'] = $values['query_parameters'];
    $this->configuration['count'] = $values['count'];
    $this->configuration['placement_name'] = $values['placement_name'];
    $this->configuration['layout'] = $values['layout'];
    $this->configuration['show_provider_branding'] = $values['show_provider_branding'];
    $this->configuration['lazy_load'] = $values['lazy_load'];

  }

  /**
   * @param $keyValueString
   * @return array
   */
  public function parseKeyValuePairs($keyValueString) {
    $key_value_list = explode(PHP_EOL, $keyValueString);
    $params = [];
    foreach($key_value_list as $param) {
      $key_value_array = explode('|', $param);
      if (count($key_value_array) === 2) {
        list($k, $v) = $key_value_array;
        $params[] = [
          'param' => $k,
          'value' => trim($v),
        ];
      }
    }
    return $params;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $uuid = $this->configuration['uuid'];
    $build['content_container'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'id' => $uuid,
        'class' => 'mp_dynamic_content',
        'title' => 'Dynamic Content Block',
      ]
    ];

    // Get advanced settings from provider term's fields.
    $tid = $this->configuration['provider_type'];
    $provider = Term::load($tid);
    $name = $provider->getName();

    $query_params = $this->parseKeyValuePairs($this->configuration['query_parameters']);
    $device_params = []; //TODO: do we need this?

    $vendor_plugin = $provider->get('field_vendor_plugin')->getValue();
    if (!isset($vendor_plugin)) {
      $vendor_plugin = 'default';
    }
    else {
      $vendor_plugin = $vendor_plugin[0]['value'];
    }

    $settings = [
      'uuid' => $uuid,
      'url' => $provider->get('field_provider_api_url')->getValue()[0]['value'],
      'jsonp' => $provider->get('field_provider_api_format')->getValue()[0]['value'] === 'json-p',
      'vendor' => $vendor_plugin,
      'placement_name' => $this->configuration['placement_name'],
      'count' => $this->configuration['count'],
      'path_to_content_list' => $provider->get('field_provider_path_content_list')->getValue()[0]['value'],
      'subpath_to_content_title' => $provider->get('field_provider_subpath_title')->getValue()[0]['value'],
      'subpath_to_content_image' => $provider->get('field_provider_subpath_image')->getValue()[0]['value'],
      'subpath_to_content_url' => $provider->get('field_provider_subpath_url')->getValue()[0]['value'],
      'chrome_links' => $provider->get('field_provider_chrome_links')->getValue()[0]['value'],
      'query_parameters' => $query_params,
      'device_parameters' => $device_params,
      'layout' => $this->configuration['layout'],
      'lazy_load' => $this->configuration['lazy_load'],
      'show_provider_branding' => $this->configuration['show_provider_branding'],
    ];

    $api_key = $provider->get('field_provider_api_key')->getValue();
    if (isset($api_key) && !empty($api_key)) {
      $settings['api_key'] = $api_key;
    }

    $build['#attached']['drupalSettings']['mp_dynamic_content']['blocks'][$uuid] = $settings;
    $build['#attached']['library'] = [
      'mp_dynamic_content/react',
      'mp_dynamic_content/mp_dynamic_content',
    ];

    return $build;
  }

}
