<?php
/**
 * @file
 * Contains \Drupal\mp_dynamic_content\Controller\ProviderController.
 */

namespace Drupal\mp_dynamic_content\Controller;

use Drupal\Core\Controller\ControllerBase;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controller routines for mp_dynamic_content routes.
 *
 * Sample usage: /mp_dynamic_content/get_provider?provider=taboola
 */
class ProviderController extends ControllerBase {

  /**
   * Callback for `mp_dynamic_content/get_provider` API method.
   */
  public function get_provider( Request $request ) {
    $data = [];
    $values = [];

    // Provider vocab.
    $vocabulary = 'providers';

    // Get term from request.
    $term_name = \Drupal::request()
      ->query->get('provider');

    if (!empty($term_name)) {
      $values['name'] = trim($term_name);
      
      $vocabularies = taxonomy_vocabulary_get_names();
      if (isset($vocabularies[$vocabulary])) {
        $values['vid'] = $vocabulary;

        // Actually retrieve the term.
        $terms = entity_load_multiple_by_properties('taxonomy_term', $values);
        
        if (!empty($terms)) {
          $term = array_pop($terms);

          // Query params.
          $query_parameters = [];
          $query_params = $term->get('field_provider_query_parameters')->getValue();
          if (!empty($query_params)) {
            $query_params = $query_params[0]['value'];
            $query_params = explode(PHP_EOL, $query_params);

            foreach ($query_params as $param) {           
              $parts = explode('|', $param);
              $query_parameters[] = [
                'param' => $parts[0],
                'value' => $parts[1],
              ];
            }
          }

          // Device params.
          $device_parameters = [];
          $device_params = $term->get('field_provider_device_parameters')->getValue();
          if (!empty($device_params)) {
            $device_params = $device_params[0]['value'];
            $device_params = explode(PHP_EOL, $device_params);

            foreach ($device_params as $param) {
              $parts = explode('|', $param);
              $device_parameters[] = [
                'param' => $parts[0],
                'value' => $parts[1],
              ];
            }
          }
          //

          $data = [
            'api_format' => $term->get('field_provider_api_format')->getValue()[0]['value'],
            'api_url' => $term->get('field_provider_api_url')->getValue()[0]['value'],
            'layout' => $term->get('field_provider_layout')->getValue()[0]['value'],
            'chrome_links' => $term->get('field_provider_chrome_links')->getValue()[0]['value'],
            'path_to_content_list' => $term->get('field_provider_path_content_list')->getValue()[0]['value'],
            'subpath_to_title' => $term->get('field_provider_subpath_title')->getValue()[0]['value'],
            'subpath_to_image' => $term->get('field_provider_subpath_image')->getValue()[0]['value'],
            'subpath_to_url' => $term->get('field_provider_subpath_url')->getValue()[0]['value'],
            'show_provider_branding' => $term->get('field_provider_show_branding')->getValue()[0]['value'],
            'query_parameters' => $query_parameters,
            'device_parameters' => $device_parameters,
          ];
        }
      }
    }

    // Return JSON response.
    return new JsonResponse([
      'data' => $data,
      'method' => 'GET',
    ]);
  }
}
