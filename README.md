## Mobile Posse Dynamic Content Module 8.x-1.x
 
### About this Module

This module provides a configurable block that fetches JSON data, parses and normalizes it
with JSON Path, and displays a list of teasers.

### Requirements

1.  TODO

### Installation

* Install as you would normally install a contributed Drupal module.
  See: https://www.drupal.org/node/895232 for further information.

### Configuration

1.  TODO

### Development on the react App

The react app resides in the js directory (/mp_dynamic_content/js), to build or make changes its required to run:

```
yarn install
```

then the app can built:

```
yarn build
```

to start the app:

```
yarn start
```

When running the app locally, it's required to mock in the js/public/index.html the drupalSettings that are passed to the front-end:

```
var drupalSettings = {
  // Settings go here.
};
```
